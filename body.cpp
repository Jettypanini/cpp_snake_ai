#include "body.h"

Body::Body(qreal pos_x, qreal pos_y, Body * next) : next(next)
{
    setRect( 0, 0, 15, 15 );
    setBrush( Qt::green );
    setPos(pos_x, pos_y);
}

void Body::setPosition(qreal pos_x, qreal pos_y)
{
    if( next != nullptr )
    {
        next->setPosition( x(), y() );
    }
    setPos( pos_x, pos_y );
}

bool Body::popLast()
{
    if( next == nullptr )
    {
        return true;
    }
    if( next->popLast() )
    {
        delete next;
        next = nullptr;
    }
    return false;
}

void Body::delete_body(int score)
{
    for( int i = 0; i <= score; i++ )
    {
        popLast();
    }
}

bool Body::check_collision(qreal pos_x, qreal pos_y)
{
    if( pos_x == this->x() && pos_y == this->y() )
    {
        return false;
    }
    else
    {
        return true;
    }
}
