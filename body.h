#ifndef BODY_H
#define BODY_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QBrush>

class Body : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
    public:
        Body( qreal pos_x = 0, qreal pos_y = 0, Body * next = nullptr);
        void setPosition( qreal pos_x, qreal pos_y );
        bool popLast();
        void delete_body( int score );
        bool check_collision( qreal pos_x, qreal pos_y );

    private:
        Body *next;
};

#endif // BODY_H
