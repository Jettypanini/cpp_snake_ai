#ifndef BOX_H
#define BOX_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QBrush>

class Box: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
    public:
       Box();
       bool check_Collision( qreal pos_x, qreal pos_y );
};

#endif // BOX_H
