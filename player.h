#ifndef PLAYER_H
#define PLAYER_H

#include <QGraphicsRectItem>
#include <QKeyEvent>

class Player : public QGraphicsRectItem
{
    private:
        char direction;
        bool newMove;
    public:
        Player();
        void keyPressEvent( QKeyEvent *event );
        char get_direction();
        void stop();
        bool getNewMove() const;
        void resetNewMove();
};

#endif // PLAYER_H
