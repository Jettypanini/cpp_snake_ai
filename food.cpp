#include "food.h"

void Food::pos_switch()
{
    items_x = rand() % 660;
    items_y = rand() % 660;

    items_x = items_x - ( items_x % 15 );
    items_y = items_y - ( items_y % 15 );

    setRect( items_x, items_y, 15, 15 );
}

Food::Food()
{
    pos_switch();
    setBrush( Qt::cyan );
}

bool Food::eaten( qreal pos_x, qreal pos_y )
{
    if( pos_x == items_x && pos_y == items_y )
    {
        return false;
    }
    else
    {
        return true;
    }
}

